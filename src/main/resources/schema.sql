DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `userlist`;
DROP TABLE IF EXISTS `calendar`;

CREATE TABLE IF NOT EXISTS `calendar` (
  `date` date DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `workHours` decimal(12,2) DEFAULT NULL
);

CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(190) NOT NULL,
    `password` VARCHAR(190) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
);


CREATE TABLE IF NOT EXISTS `userlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `payPerHour` int(11) DEFAULT NULL,
  `workHours` decimal(12,2) DEFAULT NULL,
  `gross_income` decimal(12,2) DEFAULT NULL,
  `net_income` decimal(10,0) DEFAULT NULL,
  `extra` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
);