INSERT INTO `user` (username, password) VALUES
    ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei'),
    ('peeter1', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei'),
    ('peeter2', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei'),
    ('peeter3', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei'),
    ('peeter4', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei'),
    ('peeter5', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

INSERT INTO `userlist` VALUES
	(1, 'Administraator', 'https://images.vexels.com/media/users/3/140748/isolated/preview/5b078a59390bb4666df98b49f1cdedd0-male-profile-avatar-by-vexels.png', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Peedu Esimene', 'https://images.vexels.com/media/users/3/140748/isolated/preview/5b078a59390bb4666df98b49f1cdedd0-male-profile-avatar-by-vexels.png', '2019-12-01', 25, NULL, NULL, NULL, 'T1'),
	(3, 'Peedu Teine', 'https://images.vexels.com/media/users/3/140748/isolated/preview/5b078a59390bb4666df98b49f1cdedd0-male-profile-avatar-by-vexels.png', '2019-12-01', 18, NULL, NULL, NULL, 'T1'),
	(4, 'Peedu Kolmas', 'https://images.vexels.com/media/users/3/140748/isolated/preview/5b078a59390bb4666df98b49f1cdedd0-male-profile-avatar-by-vexels.png', '2019-12-01', 15, NULL, NULL, NULL, 'T3'),
	(5, 'Peedu Neljas', 'https://images.vexels.com/media/users/3/140748/isolated/preview/5b078a59390bb4666df98b49f1cdedd0-male-profile-avatar-by-vexels.png', '2019-12-01', 12, NULL, NULL, NULL, 'T2'),
	(6, 'Peedu Viies', 'https://images.vexels.com/media/users/3/140748/isolated/preview/5b078a59390bb4666df98b49f1cdedd0-male-profile-avatar-by-vexels.png', '2019-12-01', 10, NULL, NULL, NULL, 'T2');

INSERT INTO `calendar` (`date`, `employee_id`, `workHours`) VALUES
	('2019-12-01', 2, 8.00),
	('2019-12-01', 3, 7.00),
	('2019-12-01', 4, 6.00),
	('2019-12-01', 5, 5.00),
	('2019-12-01', 6, 4.00),
	('2019-12-02', 2, 2.00),
	('2019-12-02', 3, 2.00),
	('2019-12-02', 4, 2.00),
	('2019-12-02', 5, 2.00),
	('2019-12-02', 6, 2.00);

UPDATE userlist SET userlist.workHours = (SELECT SUM(calendar.workHours) FROM calendar WHERE userlist.id=calendar.employee_id);