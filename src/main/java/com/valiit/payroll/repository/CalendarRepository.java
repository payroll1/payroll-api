package com.valiit.payroll.repository;

import com.valiit.payroll.model.PayrollCalendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@Repository
public class CalendarRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addToCalendar(PayrollCalendar payrollCalendar) {
        jdbcTemplate.update(
                "insert into calendar (date, employee_id, workHours) values (?, ?, ?)",
                payrollCalendar.getDate(), payrollCalendar.getEmployee_id(), payrollCalendar.getWorkHours()
        );
    }

    public List<PayrollCalendar> getCalendar() {
        return jdbcTemplate.query("select * from calendar", mapCalendarRows);
    }

    private RowMapper<PayrollCalendar> mapCalendarRows = (rs, rowNum) -> {
        PayrollCalendar payrollCalendar = new PayrollCalendar();
        payrollCalendar.setDate(rs.getDate("date") != null ? rs.getDate("date").toLocalDate() : null);
        payrollCalendar.setEmployee_id(rs.getInt("employee_id"));
        payrollCalendar.setWorkHours(rs.getDouble("workHours"));
        return payrollCalendar;
    };


    public void getWorkHoursByEmployee() {
        String sql = "UPDATE userlist SET userlist.workHours = (SELECT SUM(calendar.workHours) FROM calendar WHERE userlist.id=calendar.employee_id);";
        jdbcTemplate.execute(sql);
    }

    public void updatePayrollCalendar(PayrollCalendar payrollCalendar) {
        jdbcTemplate.update(
                "insert into calendar (date, employee_id, workHours) values (?, ?, ?)",
                payrollCalendar.getDate(), payrollCalendar.getEmployee_id(), payrollCalendar.getWorkHours()
        );
    }

    public List<PayrollCalendar> getCalendarData(int employee_id) {
        return jdbcTemplate.query(
                "select * from calendar where employee_id = ?",
                new Object[]{employee_id}, mapCalendarRows);
    }

    public List<PayrollCalendar> getCalendarMonth(int employee_id, LocalDate date) {
        return jdbcTemplate.query(
                "select * from calendar where employee_id = ? AND DATE < ?",
                new Object[]{employee_id, date}, mapCalendarRows);
    }

    public void deleteCalendar(int employee_id) {
        jdbcTemplate.update("delete from calendar where employee_id = ? ORDER BY date DESC LIMIT 1", employee_id);

    }

    public void deleteAllFromCalendar() {
        String delete = "TRUNCATE TABLE calendar";
        jdbcTemplate.execute(delete);

    }

}
