package com.valiit.payroll.repository;

import com.valiit.payroll.dto.JwtResponseDto;
import com.valiit.payroll.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

//    @Autowired
//    private JwtResponseDto jwtResponseDto;

    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from user where username = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }

    public void addUser(User user) {
        jdbcTemplate.update("insert into `user` (`username`, `password`) values (?, ?)", user.getUsername(), user.getPassword());
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query(
                "select * from `user` where `username` = ?",
                new Object[]{username},
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"))
        );
        return users.size() > 0 ? users.get(0) : null;
    }


    public User getUser(int id) {
        List<User> users = jdbcTemplate.query(
                "select * from user where id = ?",
                new Object[]{id},
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"))
        );
        return users.size() > 0 ? users.get(0) : null;
    }

    private RowMapper<User> mapUserRows = (rs, rowNum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setUsername(rs.getString("username"));
        return user;
    };

    public List<User> getUsers() {
        return jdbcTemplate.query("select * from user", mapUserRows);
    }

    public void updateUser(User user) {
        jdbcTemplate.update("update user set username = ?, password = ? where id = ?", user.getUsername(), user.getPassword(), user.getId());
    }

    public void deleteUser(int id) {
        jdbcTemplate.update("delete from user where id = ?", id);
    }

}














