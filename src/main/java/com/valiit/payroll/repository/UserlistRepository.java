package com.valiit.payroll.repository;

import com.valiit.payroll.model.User;
import com.valiit.payroll.model.Userlist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserlistRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CalendarRepository calendarRepository;

    public List<Userlist> getPayroll() {
        List<Userlist> userList = jdbcTemplate.query("select * from userlist", mapUserlistRows);

        for(int i = 0; i < userList.size(); i++) {
            userList.get(i).setCalendar(calendarRepository.getCalendarData(userList.get(i).getId()));
        }

        return userList;
    }

    public Userlist getUserlist(int id) {
        List<Userlist> payroll = jdbcTemplate.query(
                "select * from userlist where id = ?",
                new Object[]{id}, mapUserlistRows);
        return payroll.size() > 0 ? payroll.get(0) : null;
    }

    public boolean userlistExists(Userlist userlist) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from userlist where name = ?", new Object[]{userlist.getName()}, Integer.class);
        return count != null && count > 0;
    }

    public void addUserlist(Userlist userlist) {
        jdbcTemplate.update(
                "insert into userlist (name, logo, birthDate, payPerHour, workHours, gross_income, net_income, extra) values (?, ?, ?, ?, ?, ?, ?, ?)",
                userlist.getName(), userlist.getLogo(), userlist.getBirthDate(), userlist.getPayPerHour(), userlist.getWorkHours(),
                userlist.getGross_income(), userlist.getNet_income(), userlist.getExtra()
        );
    }

    public void updateUserlist(Userlist userlist) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getUserByUsername(username);
        jdbcTemplate.update(
                "update userlist set name = ?, logo = ?, birthDate = ?, payPerHour = ?, workHours = ?, gross_income = ?, net_income = ?, extra = ? where id = ?",
                userlist.getName(), userlist.getLogo(), userlist.getBirthDate(), userlist.getPayPerHour(), userlist.getWorkHours(),
                userlist.getGross_income(), userlist.getNet_income(), userlist.getExtra(), userlist.getId()
        );
    }

    public void deleteUserlist(int id) {
        jdbcTemplate.update("delete from userlist where id = ?", id);
    }

    private RowMapper<Userlist> mapUserlistRows = (rs, rowNum) -> {
        Userlist userlist = new Userlist();
        userlist.setId(rs.getInt("id"));
        userlist.setName(rs.getString("name"));
        userlist.setLogo(rs.getString("logo"));
        userlist.setBirthDate(rs.getDate("birthDate") != null ? rs.getDate("birthDate").toLocalDate() : null);
        userlist.setPayPerHour(rs.getInt("payPerHour"));
        userlist.setWorkHours(rs.getDouble("workHours"));
        userlist.setGross_income(rs.getDouble("gross_income"));
        userlist.setNet_income(rs.getDouble("net_income"));
        userlist.setExtra(rs.getString("extra"));

        return userlist;
    };
}