package com.valiit.payroll.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Userlist {

    private Integer id;
    private String name;
    private String logo;
    private LocalDate birthDate;
    private Integer payPerHour;
    private Double workHours;
    private Double gross_income;
    private Double net_income;
    private String extra;
    private List<PayrollCalendar> calendar = new ArrayList<>();


    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public void setPayPerHour(Integer payPerHour) {
        this.payPerHour = payPerHour;
    }

    public void setWorkHours(Double workHours) {
        this.workHours = workHours;
    }

    public void setGross_income(Double gross_income) {
        this.gross_income = gross_income;
    }

    public void setNet_income(Double net_income) {
        this.net_income = net_income;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogo() {
        return logo;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Integer getPayPerHour() {
        return payPerHour;
    }

    public Double getWorkHours() {
        return workHours;
    }

    public Double getGross_income() {
        return gross_income;
    }

    public Double getNet_income() {
        return net_income;
    }

    public String getExtra() {
        return extra;
    }

    public List<PayrollCalendar> getCalendar() {
        return calendar;
    }

    public void setCalendar(List<PayrollCalendar> calendar) {
        this.calendar = calendar;
    }
}
