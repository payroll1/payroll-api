package com.valiit.payroll.model;

import java.time.LocalDate;

public class PayrollCalendar {

    private Integer employee_id;
    private LocalDate date;
    private Double workHours;

    public Integer getEmployee_id() {
        return employee_id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Double getWorkHours() {
        return workHours;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setWorkHours(Double workHours) {
        this.workHours = workHours;
    }
}
