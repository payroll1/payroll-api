package com.valiit.payroll.service;

import com.valiit.payroll.dto.*;
import com.valiit.payroll.model.User;
import com.valiit.payroll.repository.UserRepository;
import com.valiit.payroll.util.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public GenericResponseDto register(UserRegistrationDto userRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        User user = new User(userRegistration.getId(), userRegistration.getUsername(), passwordEncoder.encode(userRegistration.getPassword()));
        if (!userRepository.userExists(userRegistration.getUsername())) {
            userRepository.addUser(user);
        } else {
            responseDto.getErrors().add("User with the specified username already exists.");
        }
        return responseDto;
    }

    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());
        final User userDetails = userRepository.getUserByUsername(request.getUsername());
        final String token = jwtTokenService.generateToken(userDetails.getUsername());
        return new JwtResponseDto(userDetails.getId(), userDetails.getUsername(), token);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public UserDto getUser(int id) {
        Assert.isTrue(id > 0, "ID of the user not specified");
        User user = userRepository.getUser(id);
        return Transformer.toUserDto(user);
    }

    public List<UserDto> getUsers() {
        return userRepository.getUsers().stream().map(Transformer::toUserDto).collect(Collectors.toList());
    }

    public void updateUser(UserRegistrationDto userRegistrationDto) {
        Assert.hasText(userRegistrationDto.getUsername(), "User name is not specified");
        User user = userRepository.getUser(userRegistrationDto.getId());
        if (user.getUsername() != null) {
            user.setPassword(passwordEncoder.encode(userRegistrationDto.getPassword()));
            user.setUsername(userRegistrationDto.getUsername());
            userRepository.updateUser(user);
        } else {
            Assert.isTrue(!userRepository.userExists(user.getUsername()), "The user with the specified name already exists");
            userRepository.addUser(user);
        }
    }

    public void deleteUser(int id) {
        if (id > 0) {
            userRepository.deleteUser(id);
        }
    }
}