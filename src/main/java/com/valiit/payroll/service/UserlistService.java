package com.valiit.payroll.service;

import com.valiit.payroll.dto.UserlistDto;
import com.valiit.payroll.model.Userlist;
import com.valiit.payroll.repository.UserlistRepository;
import com.valiit.payroll.util.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserlistService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserlistRepository userlistRepository;

    public List<UserlistDto> getPayroll() {
        return userlistRepository.getPayroll().stream().map(Transformer::toUserlistDto).collect(Collectors.toList());
    }

    public UserlistDto getUserlist(int id) {
        Assert.isTrue(id > 0, "The ID of the userlist not specified");

        Userlist userlist = userlistRepository.getUserlist(id);
        return Transformer.toUserlistDto(userlist);
    }

    public void saveUserlist(UserlistDto userlistDto) {
        Assert.notNull(userlistDto, "Name not specified");
        Assert.hasText(userlistDto.getName(), "Name not specified");

        Userlist userlist = Transformer.toUserlistModel(userlistDto);
        if (userlist.getId() != null && userlist.getId() > 0) {
            userlistRepository.updateUserlist(userlist);
        } else {
            Assert.isTrue(!userlistRepository.userlistExists(userlist), "Entry with specified name already exists!");
            userlistRepository.addUserlist(userlist);
        }
    }

    public void addUserForEmployee() {
        String sql = "insert into user (username, password)  VALUES (RAND()*1000000, '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');";
        jdbcTemplate.execute(sql);
    }

    public void deleteUserlist(int id) {
        if (id > 0) {
            userlistRepository.deleteUserlist(id);
        }
    }
}

