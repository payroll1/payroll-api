package com.valiit.payroll.service;

import com.valiit.payroll.dto.CalendarDto;
import com.valiit.payroll.model.PayrollCalendar;
import com.valiit.payroll.repository.CalendarRepository;
import com.valiit.payroll.util.Transformer;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CalendarService {

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void saveCalendar(CalendarDto calendarDto) {
        Assert.notNull(calendarDto, "Name not specified");
        PayrollCalendar payrollCalendar = Transformer.toCalendarModel(calendarDto);
        if (payrollCalendar.getEmployee_id() != null && payrollCalendar.getEmployee_id() > 0) {
            calendarRepository.updatePayrollCalendar(payrollCalendar);
        }
    }

    public void getWorkHoursByEmployee() {
        String sql = "UPDATE userlist SET userlist.workHours = (SELECT SUM(calendar.workHours) FROM calendar WHERE userlist.id=calendar.employee_id);";
        jdbcTemplate.execute(sql);
    }

    public List<CalendarDto> getCalendar() {
//        return calendarRepository.getCalendar().stream().map(Transformer::toCalendarDto).collect(Collectors.toList());
        return Transformer.toCalendarDto(calendarRepository.getCalendar());
    }


    public List<CalendarDto> getCalendarData(int employee_id) {
        Assert.isTrue(employee_id > 0, "The ID of the userlist not specified");

        List<PayrollCalendar> payrollCalendar = calendarRepository.getCalendarData(employee_id);
        return Transformer.toCalendarDto(payrollCalendar);
    }

    public List<CalendarDto> getCalendarMonth(int employee_id, LocalDate date) {
        Assert.isTrue(employee_id > 0, "The ID of the userlist not specified");

        List<PayrollCalendar> payrollCalendar = calendarRepository.getCalendarMonth(employee_id, date);
        return Transformer.toCalendarDto(payrollCalendar);
    }

    public void deleteCalendar(int employee_id) {
        if (employee_id > 0)
            calendarRepository.deleteCalendar(employee_id);
    }

}
