package com.valiit.payroll.dto;

import java.time.LocalDate;

public class CalendarDto {

    private LocalDate date;
    private Integer employee_id;
    private Double workHours;

    public LocalDate getDate() {
        return date;
    }

    public Integer getEmployee_id() {
        return employee_id;
    }

    public Double getWorkHours() {
        return workHours;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public void setWorkHours(Double workHours) {
        this.workHours = workHours;
    }
}
