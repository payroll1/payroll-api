package com.valiit.payroll.rest;

import com.valiit.payroll.dto.UserlistDto;
import com.valiit.payroll.model.PayrollCalendar;
import com.valiit.payroll.model.Userlist;
import com.valiit.payroll.repository.CalendarRepository;
import com.valiit.payroll.repository.UserlistRepository;
import com.valiit.payroll.service.UserlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/payroll")
@CrossOrigin("*")
public class UserlistController {

    @Autowired
    private CalendarRepository calendarRepository;
    @Autowired
    private UserlistService userlistService;

    @Autowired
    private UserlistRepository userlistRepository;

    @GetMapping
    public List<UserlistDto> getPayroll() {
        return userlistService.getPayroll();
    }

    @GetMapping("/{id}")
    public UserlistDto getUserlist(@PathVariable("id") int id) {
        return userlistService.getUserlist(id);
    }

    @PostMapping
    public void saveUserlist(@RequestBody UserlistDto userlistDto) {
        userlistService.saveUserlist(userlistDto);
        userlistService.addUserForEmployee();
    }

    @DeleteMapping("/{id}")
    public void deleteUserlist(@PathVariable("id") int id) {
        userlistService.deleteUserlist(id);
    }

}
