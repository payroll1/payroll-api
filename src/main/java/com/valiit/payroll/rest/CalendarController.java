package com.valiit.payroll.rest;

import com.valiit.payroll.dto.CalendarDto;
import com.valiit.payroll.repository.CalendarRepository;
import com.valiit.payroll.service.CalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/calendar")
@CrossOrigin("*")
public class CalendarController {

    @Autowired
    private CalendarService calendarService;

    @Autowired
    private CalendarRepository calendarRepository;

    @PostMapping
    public void saveCalendar(@RequestBody CalendarDto calendarDto) {
        calendarService.saveCalendar(calendarDto);
        calendarService.getWorkHoursByEmployee();
    }

    @GetMapping
    public List<CalendarDto> getCalendar() {
        return calendarService.getCalendar();
    }

    @GetMapping("/{employee_id}")
    public List<CalendarDto> getCalendarData(@PathVariable("employee_id") int employee_id) {
        return calendarService.getCalendarData(employee_id);
    }

    @GetMapping("/{employee_id}/{date}")
    public List<CalendarDto> getCalendarMonth(
            @PathVariable("employee_id") int employee_id,
            @PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        return calendarService.getCalendarMonth(employee_id, date);
    }


    @DeleteMapping("/{employee_id}")
    public void deleteCalendar(@PathVariable("employee_id") int employee_id) {
        calendarService.deleteCalendar(employee_id);
        calendarService.getWorkHoursByEmployee();
    }

    @DeleteMapping("/delete")
    public void deleteAllFromCalendar() {
        calendarRepository.deleteAllFromCalendar();
        calendarService.getWorkHoursByEmployee();
    }
}
