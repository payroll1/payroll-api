package com.valiit.payroll.rest;

import com.valiit.payroll.dto.*;
import com.valiit.payroll.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDto> getUsers() {
        return userService.getUsers();
    }

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable("id") int id) {
        return userService.getUser(id);
    }

    @PostMapping("/update")
    public void updateUser(@RequestBody UserRegistrationDto userRegistrationDto) {
        userService.updateUser(userRegistrationDto);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") int id) {
        userService.deleteUser(id);
    }
}