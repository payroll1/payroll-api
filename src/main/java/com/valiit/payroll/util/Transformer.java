package com.valiit.payroll.util;

import com.valiit.payroll.dto.CalendarDto;
import com.valiit.payroll.dto.UserDto;
import com.valiit.payroll.dto.UserlistDto;
import com.valiit.payroll.model.PayrollCalendar;
import com.valiit.payroll.model.User;
import com.valiit.payroll.model.Userlist;

import java.util.ArrayList;
import java.util.List;

public class Transformer {

    public static Userlist toUserlistModel(UserlistDto initialObject) {
        if (initialObject == null) {
            return null;
        }

        Userlist resultingObject = new Userlist();
        resultingObject.setId(initialObject.getId());
        resultingObject.setName(initialObject.getName());
        resultingObject.setLogo(initialObject.getLogo());
        resultingObject.setBirthDate(initialObject.getBirthDate());
        resultingObject.setPayPerHour(initialObject.getPayPerHour());
        resultingObject.setWorkHours(initialObject.getWorkHours());
        resultingObject.setGross_income(initialObject.getGross_income());
        resultingObject.setNet_income(initialObject.getNet_income());
        resultingObject.setExtra(initialObject.getExtra());

        return resultingObject;
    }

    public static User toUserModel(UserDto initialObject) {
        if (initialObject == null) {
            return null;
        }

        User resultingObject = new User();
        resultingObject.setId(initialObject.getId());
        resultingObject.setUsername(initialObject.getUsername());
        resultingObject.setPassword(initialObject.getPassword());
        return resultingObject;
    }


    public static UserlistDto toUserlistDto(Userlist initialObject) {
        if (initialObject == null) {
            return null;
        }

        UserlistDto resultingObject = new UserlistDto();
        resultingObject.setId(initialObject.getId());
        resultingObject.setName(initialObject.getName());
        resultingObject.setLogo(initialObject.getLogo());
        resultingObject.setBirthDate(initialObject.getBirthDate());
        resultingObject.setPayPerHour(initialObject.getPayPerHour());
        resultingObject.setWorkHours(initialObject.getWorkHours());
        resultingObject.setGross_income(initialObject.getGross_income());
        resultingObject.setNet_income(initialObject.getNet_income());
        resultingObject.setExtra(initialObject.getExtra());

        if (initialObject.getPayPerHour() > 0 && initialObject.getWorkHours() > 0) {
            Double Gross_income = initialObject.getPayPerHour() * initialObject.getWorkHours();
            resultingObject.setGross_income(Gross_income);
        }

        if (initialObject.getPayPerHour() > 0 && initialObject.getWorkHours() > 0) {
            Double Net_income = 0.765 * (initialObject.getPayPerHour() * initialObject.getWorkHours());
            resultingObject.setNet_income(Net_income);
        }

        resultingObject.setCalendar(toCalendarDto(initialObject.getCalendar()));

        return resultingObject;
    }

    public static UserDto toUserDto(User initialObject) {
        if (initialObject == null) {
            return null;
        }
        UserDto resultingObject = new UserDto();
        resultingObject.setId(initialObject.getId());
        resultingObject.setUsername(initialObject.getUsername());
        return resultingObject;
    }

    public static List<CalendarDto> toCalendarDto(List<PayrollCalendar> initialObject) {
        List<CalendarDto> resultingObjects = new ArrayList<>();

        for(int i = 0; i < initialObject.size(); i++) {
            CalendarDto resultingObject = new CalendarDto();
            resultingObject.setEmployee_id(initialObject.get(i).getEmployee_id());
            resultingObject.setDate(initialObject.get(i).getDate());
            resultingObject.setWorkHours(initialObject.get(i).getWorkHours());
            resultingObjects.add(resultingObject);
        }

        return resultingObjects;
    }


    public static PayrollCalendar toCalendarModel(CalendarDto initialObject) {
        if (initialObject == null) {
            return null;
        }

        PayrollCalendar resultingObject = new PayrollCalendar();
        resultingObject.setDate(initialObject.getDate());
        resultingObject.setEmployee_id(initialObject.getEmployee_id());
        resultingObject.setWorkHours(initialObject.getWorkHours());

        return resultingObject;
    }
}
